VAR.(
(let rec (a, b) = (5, fun x -> if x = 0 then 1 else x * b (x - 1)) in b a);
let f (y, z) w = match w with
  | Cons x -> x :: y
  | Nil -> z
in f (y, z) w
)