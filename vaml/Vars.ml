let botvar_unbox = Failure "Attempted unboxing of empty variable"

module type VARIABLE =
  sig
    type 'a varimpl
    type 'a var = 'a Lazy.t varimpl
    val box : 'a -> 'a var
    val unbox : 'a var -> 'a
    val assign : 'a var -> 'a -> unit
    val botvar : unit -> 'a var
  end

module DummyVar : VARIABLE =
  struct
    type 'a varimpl = 'a
    type 'a var = 'a Lazy.t varimpl
    let box x = lazy x
    let unbox x = Lazy.force x
    let assign _ _ = ()
    let botvar () = lazy (raise botvar_unbox)
  end

module RefVar : VARIABLE =
  struct
    type 'a varimpl = 'a ref
    type 'a var = 'a Lazy.t varimpl
    let box x = ref (lazy x)
    let unbox v = Lazy.force (!v)
    let assign v x = v := (lazy x)
    let botvar () = ref (lazy (raise botvar_unbox))
  end

module ObjVar : VARIABLE =
  struct
    class ['a] variable x =
      object
        val mutable value = (x : 'a)
        method setval newval = value <- newval
        method getval () = value
      end
    type 'a varimpl = 'a variable
    type 'a var = 'a Lazy.t varimpl
    let box x = new variable (lazy x)
    let unbox v = Lazy.force (v#getval())
    let assign v x = v#setval (lazy x)
    let botvar () = new variable (lazy (raise botvar_unbox))
  end

module VarImpl = ObjVar
