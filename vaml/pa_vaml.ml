module Id = struct
  let name = "pa_vaml"
  let version = "1.0"
end

open Camlp4

module Make (Syntax : Sig.Camlp4Syntax) =
  struct
    open Sig
    include Syntax

    let printer = let module P = Camlp4.Printers.OCaml.Make(Syntax)
               in new P.printer ()

  (* Transform a formatter function into a to_string function. *)
   let format_to_string (f : Format.formatter -> 'a -> unit) (v : 'a) : string =
   let buf = Buffer.create 128 in
   let fmt = Format.formatter_of_buffer buf in
   let () = f fmt v in
   let () = Format.pp_print_flush fmt () in
      Buffer.contents buf

    (* Some to_string functions for particular AST types. *)
    let expr_of_string : Ast.expr -> string = format_to_string printer#expr
    let patt_of_string : Ast.patt -> string = format_to_string printer#patt
    let ident_of_string : Ast.ident -> string = format_to_string printer#ident
    let binding_of_string : Ast.binding -> string = format_to_string printer#binding

    let add_val x = x ^ "_val"

    class ['a] varify (_loc : Loc.t) = object (self)
      inherit Ast.map as super
      method _Loc_t (_ : 'a) = _loc

      val varmod = <:expr< Vars.VarImpl >>
      method unbox e = <:expr< $varmod$ . unbox( $e$ ) >>
      method box e = <:expr< $varmod$ . box( $e$ ) >>
      method assign v = <:expr< $varmod$ . assign ( $v$ ) >>
      method assign_to v e = <:expr< $varmod$ . assign ( $v$ ) ( $e$ ) >>
      method botvar () = <:expr< ( $varmod$ . botvar ) () >>

      method map_ident f = function
        | <:ident< $a$ . $b$ >> -> <:ident< $self#map_ident f a$ . $self#map_ident f b$ >>
        | Ast.IdApp(l, a, b) -> Ast.IdApp(l, self#map_ident f a, self#map_ident f b)
        | Ast.IdLid(l, i) -> Ast.IdLid(l, f i)
        (* | <:ident< $uid:i$ >> -> <:ident< $uid:(f i))$ >> *)
        | x -> x

      method map_ret_patt_ids f pattern idlist =
        let proc = self#map_ret_patt_ids f in
        let seqproc pf p1 p2 =
          let (pp1, ids1) = proc p1 idlist in
          let (pp2, ids2) = proc p2 ids1 in
          (pf pp1 pp2, ids2) in
        let recproc pf p =
          let (pp, ids) = proc p idlist in (pf pp, ids)
        in match pattern with
          | <:patt< $lid:i$ >> -> let i_id = <:ident< $lid:i$ >> in ((<:patt< $id:(f i_id)$ >>), i_id::idlist)
          | <:patt< #$i$ >> -> ((<:patt< $id:(f i)$ >>), i::idlist)
          | <:patt< [| $p$ |] >> -> recproc (fun x -> <:patt< [| $x$ |] >>) p
          | Ast.PaCom(l, p1, p2) -> seqproc (fun x y -> Ast.PaCom(l, x, y)) p1 p2
          | <:patt< $p1$; $p2$ >> -> seqproc (fun x y -> <:patt< $x$ ; $y$ >>) p1 p2
          | <:patt< lazy $p$ >> -> recproc (fun x -> <:patt< lazy $x$ >>) p
          | <:patt< { $p$ } >> ->  recproc (fun x -> <:patt< { $x$ } >>) p
          | <:patt< ( $tup:p$ ) >>  -> recproc (fun x -> <:patt< ( $tup:x$ ) >>) p
          | <:patt< $p1$ | $p2$ >> -> seqproc (fun x y -> <:patt< $x$ | $y$ >>) p1 p2
          | Ast.PaApp (l, p1, p2) -> seqproc (fun x y -> Ast.PaApp (l, x, y)) p1 p2
          | Ast.PaAli (l, p1, p2) -> seqproc (fun x y -> Ast.PaAli (l, x, y)) p1 p2
          | Ast.PaRng (l, p1, p2) -> seqproc (fun x y -> Ast.PaRng (l, x, y)) p1 p2
          | Ast.PaLab (l, s, p) -> recproc (fun x -> Ast.PaLab (l, s, x)) p
          | Ast.PaOlb (l, s, p) -> recproc (fun x -> Ast.PaOlb (l, s, x)) p
          (*
            <:patt< ?s:(p = e) >> <:patt< ?(p = e) >> 
            <:patt< i = p >>  
            <:patt< (p : t) >>  
            <:patt< $list:x$ >> 
            <:patt< (module m) >>*)
          | _ -> (pattern, idlist)

      method expr_f fl fr expression =
        let proc ex = (match ex with
          | <:expr< $lid:_$ >> -> fr ex
          | _ -> self#expr ex) in
        match expression with (*let () = print_endline (expr_of_string ex) in*)
        (*print_endline (expr_of_string e1 ^ " ,,, " ^ expr_of_string e2) ;  *)
          | <:expr< $lid:_$ >>
            -> fr expression
          | <:expr< ( <-- ) $lid:v$ >> -> fl (<:expr< $lid:v$ >>)
          | <:expr< ( <-- ) $lid:v$ $e$ >> -> let flv = fl (<:expr< $lid:v$ >>) in <:expr< $flv$ $proc e$ >>
          | <:expr< ( <-- ) $_$ >> -> raise (Failure "Left hand side of <-- must be a variable")
          | <:expr< ( <-- ) $_$ $_$ >> -> raise (Failure "Left hand side of <-- must be a variable")
          | <:expr< $e1$ $e2$ >> -> <:expr< $proc e1$ $proc e2$ >>
          | <:expr< $e1$ . ($e2$) >> -> <:expr< $proc e1$ . ($proc e2$) >>
          | <:expr< [| $e$ |] >> -> <:expr< [| $proc e$ |] >>
          | <:expr< $e1$ ; $e2$ >> -> <:expr< $proc e1$ ; $proc e2$ >>
          | <:expr< assert $e$ >> -> <:expr< assert $proc e$ >>
          | <:expr< $e1$ := $e2$ >> -> <:expr< $proc e1$ := $proc e2$ >>
          | <:expr< if $e1$ then $e2$ else $e3$ >> -> <:expr< if $proc e1$ then $proc e2$ else $proc e3$ >>
          | Ast.ExLab (l, s, e) -> Ast.ExLab (l, s, proc e)
          | Ast.ExOlb (l, s, e) -> Ast.ExOlb (l, s, proc e)
          | <:expr< let $b$ in $e$ >> -> <:expr< let $self#binding_f self#let_binding b$ in $proc e$ >>
          | <:expr< let rec $b$ in $e$ >> -> <:expr< let rec $self#binding_f self#letrec_binding b$ in $proc e$ >>
          | <:expr< lazy $e$ >> -> <:expr< lazy ($proc e$) >>
          | <:expr< $seq:e$ >> -> <:expr< $seq:(proc e)$ >>
          | <:expr< $e$ # $s$ >> -> <:expr< $proc e$ # $s$ >>
          | <:expr< $e1$.[ $e2$ ] >> -> <:expr< $proc e1$.[ $proc e2$ ] >>
          | <:expr< $e1$, $e2$ >> -> <:expr< $proc e1$, $proc e2$ >>
          | <:expr< ( $tup:e$ ) >> -> <:expr< ( $tup:(proc e)$ ) >>
          | <:expr< try $e$ with $match_case:a$ >> -> <:expr< try $proc e$ with $match_case:(self#match_case a)$ >>
          | <:expr< match $e$ with $a$ >> -> <:expr< match $proc e$ with $self#match_case a$ >>
          | Ast.ExFun(l, m) -> Ast.ExFun(l, self#match_case m)
          (* | <:expr< $id:_$ . $id:_$ >> *)
          (* | <:expr< ($e$ : $t$) >> <:expr< ($e$ : $t1$ :> $t2$) >> *)
          (*| <:expr< for $s$ = $e1$ to $e2$ do { $e3$ } done >> -> <:expr< for $s$ = $proc e1$ to $proc e2$ do { $proc e3$ } done >> *)
          (* | <:expr< let module $s$ = $me$ in $e$ >> *)
          (* <:expr< object (($p$))? ($cst$)? end >> Object declaration  ExObj of Loc.t and patt and class_str_item *)
          (* | <:expr< {< $b$ >} >> <:expr< { $b$ } >> <:expr< { ($e$) with $b$ } >> *)
          (* | <:expr< do { $e$ } >> -> <:expr< do { $proc e$ } >> *)
          (* <:expr< ($e$ : $t$) >> *)
          (*| <:expr< while $e1$ do { $e2$ } done >> -> <:expr< while $proc e1$ do { $proc e2$ } done >>*)
          (* | <:expr< $list:x$ >> -> ex *)
          | _ -> super#expr expression

      method expr expression = self#expr_f self#assign self#unbox expression

      method tup_e = function
        | <:expr< $id:i$ >> -> <:expr< $id:i$ >>
        | e -> <:expr< $tup:e$ >>

      method tup_p = function
        | <:patt< $id:i$ >> -> <:patt< $id:i$ >>
        | p -> <:patt< $tup:p$ >>

      method let_binding p e = match p with
        | <:patt< $id:_$ >> -> <:binding< $p$ = $self#box (self#expr e)$ >>
        | _ -> let (pp, ids) = self#map_ret_patt_ids (self#map_ident add_val) p [] in
               let val_ids = List.map (self#map_ident add_val) ids in
               let innerbind = <:binding< $pp$ = $self#expr e$ >> in
               let outerpatt = self#tup_p (Ast.paCom_of_list (List.map (fun i -> <:patt< $id:i$ >>) ids)) in
               let outerexpr = self#tup_e (Ast.exCom_of_list (List.map (fun i -> self#box (<:expr< $id:i$ >>)) val_ids)) in
               <:binding< $outerpatt$ = let $innerbind$ in $outerexpr$ >>

      method letrec_binding p e = match p with
        | <:patt< $id:i$ >> -> ( 
          let ei = <:expr< $id:i$ >> in
          <:binding< $p$ = let $p$ = $self#botvar ()$ in ( $self#assign ei$ $self#expr e$ ; $ei$ ) >>)
        | _ -> let (pp, ids) = self#map_ret_patt_ids (self#map_ident add_val) p [] in
               let val_ids = List.map (self#map_ident add_val) ids in
               let idpatt = self#tup_p (Ast.paCom_of_list (List.map (fun i -> <:patt< $id:i$ >>) ids)) in
               let idex = self#tup_e (Ast.exCom_of_list (List.map (fun i -> <:expr< $id:i$ >>) ids)) in
               let bvars = self#tup_e (Ast.exCom_of_list (List.map (fun _ -> self#botvar ()) ids)) in
               let innerbind = <:binding< $pp$ = $self#expr e$ >> in
               let patches = Ast.exSem_of_list (List.map (fun (i, iv) -> self#assign_to (<:expr< $id:i$ >>) (<:expr< $id:iv$ >>)) (List.combine ids val_ids)) in
               <:binding< $idpatt$ = let $idpatt$ = $bvars$ in let $innerbind$ in ( $patches$ ; $idex$ )>>

      method binding_f f bind =
        let proc = self#binding_f f in
        match bind with
          | <:binding< $b1$ and $b2$ >> -> <:binding< $proc b1$ and $proc b2$ >>
          | <:binding< () = $exp:e$ >> -> <:binding< () = $self#expr e$ >>
          | <:binding< _ = $exp:e$ >> -> <:binding< _ = $self#expr e$ >>
          | <:binding< $pat:p$ = $exp:e$ >> -> f p e
          | _ -> bind

      method match_case cases =
        let proc = self#match_case in
        match cases with
          | <:match_case< $mc1$ | $mc2$ >> -> <:match_case< $proc mc1$ | $proc mc2$ >>
          | <:match_case< $p$ when $e1$ -> $e2$ >> -> (
            let (ee1, ee2) = (self#expr e1, self#expr e2) in
            let (pp, ids) = self#map_ret_patt_ids (self#map_ident add_val) p [] in
              match ids with
              | [] -> <:match_case< $p$ when $ee1$ -> $ee2$ >>
              | [i] -> let mie = self#box (<:expr< $id:(self#map_ident add_val i)$ >>) in
                  <:match_case< $pp$ when $ee1$ -> let $id:i$ = $mie$ in $ee2$ >>
              | _ -> (
                let val_ids = List.map (self#map_ident add_val) ids in
                let lb = Ast.paCom_of_list (List.map (fun i -> <:patt< $id:i$ >>) ids) in
                let lbind = self#tup_p lb in
                let rbind = self#tup_e (Ast.exCom_of_list (List.map (fun i -> self#box (<:expr< $id:i$ >>)) val_ids)) in
                let bind = <:binding< $lbind$ = $rbind$ >> in
                <:match_case< $pp$ when $ee1$ -> let $bind$ in $ee2$ >>
          ))
          | _ -> super#match_case cases
    end;;

    EXTEND Gram
      GLOBAL: expr;

      expr: LEVEL "simple"
      [ [ "VAR"; "."; "("; e = SELF; ")" -> (new varify _loc)#expr e ] ] ;
    END
  end

let module M = Register.OCamlSyntaxExtension(Id)(Make) in ()
