
VAR.(
x y z w; (x, y, z); (A.x + 0 + (0 + 42)) + 0 ; [x; y; z]
)

VAR.(
fun x -> x + y
)

VAR.(
x := y z w
)

VAR.(
let (x1, x2, x3) = w y z
and [a1, a2, a3] = b c d
in f h
)