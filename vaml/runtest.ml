#load "pa_vaml.cmo";;

let sum = Vars.VarImpl.box (fun x y -> x + y)
let mult = Vars.VarImpl.box (fun x y -> x * y)
let eq = Vars.VarImpl.box (fun x y -> x = y)
let print = Vars.VarImpl.box print_endline
let string_of = Vars.VarImpl.box string_of_int
let map = Vars.VarImpl.box List.map
;;

VAR.(
let x = 3 in x <-- sum x 1 ; print (string_of (sum x x));

let y = 5 in y <-- sum x y ; print (string_of (sum x y));

let rec addone = function
  | [] -> []
  | z::zs -> z <-- sum z 1 ; z :: (addone zs)
in map (fun z -> print (string_of z)) (addone [1;2;3]);

let rec fact n = if eq n 0 then 1 else
                 (let res = (print "Did a multiply" ; mult n (fact (sum n (-1)))) in
                    let f = fact in fact <-- (fun x -> if eq x n then res else f x); res)
in (print (string_of (fact 5)) ; print (string_of (fact 6)))
)
